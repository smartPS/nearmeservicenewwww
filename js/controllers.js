//angular.module('starter.controllers', ['cordovaGeolocationModule'])


app.controller('AppCtrl', function($scope, $ionicModal, $timeout,$localstorage,$ionicSideMenuDelegate,cordovaGeolocationService) {

 /*alert('hii app')
  console.log('hii dash')*/
    /*cordovaGeolocationService.getCurrentPosition(function(position){
        alert(
            'Latitude: '          + position.coords.latitude          + '\n' +
            'Longitude: '         + position.coords.longitude         + '\n' +
            'Altitude: '          + position.coords.altitude          + '\n' +
            'Accuracy: '          + position.coords.accuracy          + '\n' +
            'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
            'Heading: '           + position.coords.heading           + '\n' +
            'Speed: '             + position.coords.speed             + '\n' +
            'Timestamp: '         + position.timestamp                + '\n'
        );
    });*/

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
$scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
 $scope.toggleLeftSideMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };

            $scope.tourist_count = 5;
            $scope.auth = false;
           var user = $localstorage.getObject('user');   
           var user_check = angular.equals({}, user);
            if(user_check == false){
              $scope.auth = true;
              $scope.user = user;
              console.log('user'+JSON.stringify(user))
            }else{
              $scope.auth = false;
            }

            $scope.logout = function () {
              $localstorage.setObject('user',{});
                 var user = $localstorage.getObject('user');   
                 var user_check = angular.equals({}, user);
                if(user_check == false){

                    $scope.auth = true;
                    $scope.user = user;

                  }else{
                    window.location.reload();
                    $scope.auth = false;

                  }  
            }      


   $scope.menus = [{'href':'#/app/search','icon':'ion-search','label':'Near Me','class':''},
                   {'href':'#/app/tourists','icon':'ion-android-car','label':'Tourists','class':''},
                   {'href':'#/app/special/beauty_tips','icon':'ion-ios-rose','label':'Beauty Tips','class':''},
                   {'href':'#/app/special/maruthuva_kuripu','icon':'ion-medkit','label':'Maruthuva Kuripu','class':''},
                   {'href':'#/app/special/kids_story','icon':'ion-help-buoy','label':'Story zone','class':''},
                   {'href':'#/app/news','icon':'ion-speakerphone','label':'News','class':''},
                   {'href':'#/app/post_listing','icon':'ion-email','label':'Post your Listing','class':'nms-bg'}];
    $scope.nearme = main_url + "asset/img/icon.png"; 
    $scope.userpic = main_url + "asset/img/user.png";                
})
app.controller('touristCntrl', function($scope, $stateParams,factory,$ionicLoading) {

  var id = $stateParams.id;

   
  factory.post_List_obj(url_api,{'table':'tourist','id':id}).then(function(result){
                      $ionicLoading.hide();
                      $scope.item = result.list;
                      
           }); 

   $scope.whatsappShare=function(msg,img){
     factory.social('whatsapp',msg,img);
    }
    
     $scope.OtherShare=function(msg,img){
       factory.social('other',msg,img);
    }

})
app.controller('touristsCntrl', function ($scope,factory,$stateParams,$ionicLoading) {
            
           factory.post_List_obj(url_api,{'table':'tourist','count':'get'}).then(function(result){
                       $ionicLoading.hide();
                       $scope.items = result.list;
                      
                       
           });

})

app.controller('newsCntrl', function ($scope,factory,$stateParams,$ionicLoading,$localstorage) {
           
           var getLoc =  $localstorage.getObject('getLoc');  
           factory.post_List_obj(url+"/get_news",getLoc).then(function(result){
                       $ionicLoading.hide();
                       $scope.newss = result.list;
                      
                       
           });

})
app.controller('newsDetailsCntrl', function($scope, $stateParams,factory,$ionicLoading) {

  
  var id = $stateParams.id;

  
   
  factory.post_List_obj(url+"/get_news_details",{'id':id}).then(function(result){
                      $ionicLoading.hide();
                      $scope.item = result.list;
                      
           }); 
  
  //var link = "https://play.google.com/store/apps/details?id=com.spsolutions.nearmeservices";
    $scope.whatsappShare=function(item){
     factory.social('whatsapp',item);
    }
    
     $scope.OtherShare=function(item){
       factory.social('other',item);
    }

})
app.controller('searchCntrl', function ($scope,factory,$stateParams,$ionicLoading,$localstorage,$ionicPlatform,$state,location) {
          
           var getlocation = function(getLoc) {
              factory.post_List_obj(url+"/get_location",getLoc).then(function(result){
                        // $ionicLoading.show({duration:3000,template:'<img src="../img/logo.png">'});
                        $ionicLoading.hide();
                        $scope.locations = result.list;
                        $scope.location = result.current.id;
                        
             });

             factory.post_List_obj(url+"/get_services",getLoc).then(function(result){
                        // $ionicLoading.show({duration:3000,template:'<img src="../img/logo.png">'});
                        $ionicLoading.hide();
                        $scope.services = result.list;
                        
             });
          }


           var getLoc =  $localstorage.getObject('getLoc');
           if(angular.equals({}, getLoc)){
             var options = {enableHighAccuracy: true ,timeout :100000};
             navigator.geolocation.getCurrentPosition(successGeo,errorGeo,options); 
             
           }else{
           
            getlocation(getLoc);
           }

           



           function successGeo (pos){
             var geo = {'latitude':pos.coords.latitude,'longitude' : pos.coords.longitude};
             $localstorage.setObject('getLoc',geo);
             //alert("geo"+JSON.stringify(getLoc));
             getlocation(geo);
             
           }
            
            function errorGeo (error) {
              //for location function not working mobiles
              var geo = {'latitude':"9.174381384620009",'longitude' : "77.53262055499272"};
              $localstorage.setObject('getLoc',geo);
              getlocation(geo);
            }

           

           $scope.change_location =  function (location) {
              factory.post_List_obj(url+"/get_location_geo",{'location_id':location}).then(function(result){
                      // $ionicLoading.show({duration:3000,template:'<img src="../img/logo.png">'});
                      $ionicLoading.hide();
                      //$scope.services = result.list;
                       $localstorage.setObject('getLoc',{'latitude':result.list.latitude,'longitude' : result.list.longitude});
                       var chggeoLoc =  $localstorage.getObject('getLoc');
                       factory.post_List_obj(url+"/get_services",chggeoLoc).then(function(result){
                      // $ionicLoading.show({duration:3000,template:'<img src="../img/logo.png">'});
                                $ionicLoading.hide();
                                $scope.services = result.list;
                                
                     });
             });
           }

           

})

app.controller('listingCntrl', function($scope, $stateParams,factory,$ionicLoading,$localstorage) {
  
  
  $scope.found = main_url + "asset/img/found.png";
  $scope.no_img = main_url + "asset/img/icon.png"; 
  var id = $stateParams.id;
  var getLoc =  $localstorage.getObject('getLoc'); 
  getLoc.id = id;

  getLoc.needed = 'api';
  
  var get_data = function(id) {
    factory.post_List_obj(url+"/listing",getLoc).then(function(result){
                       $ionicLoading.hide();
                      $scope.listings = result.list;
                      $scope.title = result.list[0].service_category;
                      
           }); 
  }

   get_data(getLoc);

  $scope.listings_refresh = function () {
      get_data(getLoc);

  }

})

app.controller('ownlistingDetailCntrl', function($scope, $stateParams,factory,$ionicLoading,$localstorage) {
  
  
  $scope.found = main_url + "asset/img/found.png"; 
  var id = $stateParams.id;
  var getLoc =  $localstorage.getObject('getLoc'); 
  getLoc.id = id;
  getLoc.needed = 'api';
  
  var get_data = function() {
    factory.post_List_obj(url+"/get_own_list_details",getLoc).then(function(result){
                       $ionicLoading.hide();
                      $scope.listings = result.list;
                      
           }); 
  }

   get_data();

  $scope.listings_refresh = function () {
      get_data(getLoc);

  }

})

app.controller('listingDetailCntrl', function($scope, $stateParams,factory,$ionicLoading,$localstorage,$ionicModal,$cordovaToast) {
  
   var id = $stateParams.id;
   var getLoc =  $localstorage.getObject('getLoc'); 
   getLoc.id = id;
   var user_img = main_url + "asset/img/user.png"; 
  factory.post_List_obj(url+"/get_listing_detail",getLoc).then(function(result){
                      $ionicLoading.hide();
                      $scope.item = result.list;
                      if($scope.item.profile_picture == '' && $scope.item.profile_picture !=null){
                         $scope.prof_img = user_img;
                      }   else{
                         $scope.prof_img = $scope.item.profile_picture;
                      }
           }); 

  $ionicModal.fromTemplateUrl('templates/playlist.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

  $scope.getEnquiry = function (regData) {
       regData.list_id = id;
       regData.enquiry_for = "listing";
      factory.post_List_obj(url+"/set_enquiry_detail",regData).then(function(result){
                      $ionicLoading.hide();
                      //$scope.item = result.list;
                      $scope.closeModal();
                      $cordovaToast.showLongCenter("Your enquiry has been sent");
           }); 
  }

  $scope.whatsappShare=function(msg,img){
     factory.social('whatsapp',msg,img);
    }
    
     $scope.OtherShare=function(msg,img){
       factory.social('other',msg,img);
    }

    $scope.GotoLink = function (link) {
      window.open(link,'_system');
    }


})

app.controller('specialCntrl', function($scope, $stateParams,factory,$ionicLoading) {

  var id = $stateParams.spl;

  $scope.special = id.replace("_"," ");
   
  factory.post_List_obj(url+"/get_spl_word",{'special':id}).then(function(result){
                       $ionicLoading.hide();
                       if(result.status == 'error'){
                         $scope.listings = result.list; 
                       }else{
                         $scope.listings = result.list;  
                       }
                      
                      console.log(result.list);
                      
           }); 

})

app.controller('specialDetailCntrl', function($scope, $stateParams,factory,$ionicLoading) {

  var id = $stateParams.id;

 
   
  factory.post_List_obj(url+"/get_spl_word_detail",{'special_id':id}).then(function(result){
                       $ionicLoading.hide();
                       if(result.status == 'error'){
                         $scope.listings = result.list; 

                       }else{
                         $scope.listings = result.list;  
                          $scope.category = result.list.category.replace("_"," ");
                       }
                      
                      console.log(result.list);
                      
           });
  $scope.whatsappShare=function(msg){
     factory.social('whatsapp',msg);
    }
    
     $scope.OtherShare=function(msg){
       factory.social('other',msg);
    }          

})

app.controller('post_listingCntrl', function($scope, $stateParams,factory,$ionicLoading,$ionicPopup,
              $localstorage,$ionicActionSheet,  $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice,location,
              $cordovaToast,$state,$timeout) {

       var getLoc =  $localstorage.getObject('getLoc');
       var device_id = $localstorage.get('device_token');
       $scope.found = main_url + "asset/img/found.png"; 
 
             $scope.auth = false;   
             $scope.post_list =false;      
           // $localstorage.setObject('user',{})
            var user = $localstorage.getObject('user');   
           // console.log(Object.keys(user).length);
            var user_check = angular.equals({}, user);
            if(user_check == false){
              $scope.auth = true;
              console.log(JSON.stringify(user));
            }else{
              $scope.auth = false;
             // $localstorage.setObject('user',{})
              
              $scope.login = true;
              $scope.reg = false;
            }

       $scope.register_open = function () {
              $scope.auth = false;
              $scope.login = false;
              $scope.reg = true;
       } 

       $scope.loginOpen = function () {
              $scope.auth = false;
              $scope.login = true;
              $scope.reg = false;
       } 
 
       $scope.doLogin = function (loginData) {
                loginData.device_id = device_id;
                factory.post_List_obj(url+"/login_app_customer",loginData).then(function(result){
                       $ionicLoading.hide();
                       if(result.status == 'error'){
                           $scope.showPopup = function() {
                                $ionicPopup.alert({
                                  title: 'Login Message',
                                  template: 'Error'
                                });
                                
                              };
                       }else{
                          var alertPopup = $ionicPopup.alert({
                                  title: 'Login Message',
                                  template: 'Success'
                                });
                                alertPopup.then(function(res) {
                                  console.log('Thank you for not eating my delicious ice cream cone');
                                });
                              $scope.regData = {};
                                
                                $localstorage.setObject('user', result.list);   
                                $scope.auth = true;

                                $scope.dash = false; 
                                $scope.post_list =false; 
                                $scope.post_form = true;

                                //$state.go($state.current, {}, {reload: true});
                                 window.location.reload();
                          
                       }
                      
           }); 
       }

       $scope.doReg = function (regData) {
            regData.device_id = device_id;
            factory.post_List_obj(url+"/new_app_customer",regData).then(function(result){
                       $ionicLoading.hide();
                       if(result.status == 'error'){
                           $scope.showPopup = function() {
                                var alertPopup = $ionicPopup.alert({
                                  title: 'Register Message',
                                  template: 'Error'
                                });
                                alertPopup.then(function(res) {
                                  console.log('Thank you for not eating my delicious ice cream cone');
                                });
                              };
                       }else{
                          var alertPopup = $ionicPopup.alert({
                                  title: 'Register Message',
                                  template: 'Success'
                                });
                                alertPopup.then(function(res) {
                                  console.log('Thank you for not eating my delicious ice cream cone');
                                });
                             $scope.regData = {};
                              $localstorage.setObject('user', result.list);   
                            
                              $scope.auth = true;
                              $scope.dash = false; 
                                $scope.post_list =false; 
                                $scope.post_form = true;
                           
                              
                       }
                      
                     
                      
           }); 
       }
       
        factory.post_List_obj(url+"/get_all_services",{'table':'services','count':'get'}).then(function(result){
                  // $ionicLoading.show({duration:3000,template:'<img src="../img/logo.png">'});
                  $ionicLoading.hide();
                  $scope.categories = result.list;
                  
       });
       $scope.no_image = main_url + "asset/img/found.png"; 
       factory.post_List_obj(url+"/own_listing",{'needed':'api','user':user.id}).then(function(result){
                  // $ionicLoading.show({duration:3000,template:'<img src="../img/logo.png">'});
                  $ionicLoading.hide();
                  $scope.own_listing = result.list;

                  
       });

       factory.post_List_obj(url+"/customer_data",{'needed':'api','user':user.id}).then(function(result){
                  // $ionicLoading.show({duration:3000,template:'<img src="../img/logo.png">'});
                  $ionicLoading.hide();
                  $scope.customer = result.list;

                  
       });

       $scope.open_post = function (){
         $scope.post_list =false; 
         $scope.post_form = true;
         $scope.dash = false;
         console.log('hi');
       }
       $scope.open_list = function (){
         $scope.post_form = false;
         $scope.post_list =true; 
         $scope.dash = false;
         console.log('hi');
       }
       $scope.open_dash = function (){
         $scope.dash = true; 
         $scope.post_list =false; 
         $scope.post_form = false;
         console.log('hi');
       }

       


       $scope.doService = function (serviceData) {
                var user = $localstorage.getObject('user');   
                serviceData.service_customer = user.id; 

                var urls = url + '/upload_ad_request_image';
                // File for Upload
                var targetPath = $scope.pathForImage($scope.model_image);
                // File name only
                var filename = $scope.model_image;
                var options = {
                  fileKey: "file",
                  fileName: filename,
                  chunkedMode: false,
                  mimeType: "multipart/form-data",
                  params: {
                    'service_title': serviceData.service_title,
                    'service_customer': serviceData.service_customer,
                    'service_category': serviceData.service_category,
                    'service_mobile': serviceData.service_mobile  
                  }
                };

               $cordovaFileTransfer.upload(urls, targetPath, options).then(function(result) {
                  var out = result.response;
                  //alert(JSON.stringify(result));
                  //alert(JSON.stringify(out));
                  //$ionicLoading.hide();
                  //$cordovaToast.showLongCenter("Your photo has been uploaded");
                  //alert(result.response.status);
                  var status =  (out.split('_')[0]);
                  var message = (out.split('_')[1]);
                  //alert('status'+status);
                 if(status == 'error'){
                        //$scope.results = result;
                             $ionicPopup.alert({
                                  scope : $scope,
                                  title: 'Error Message',
                                  template: message
                                });
                       }else{
                          $ionicPopup.alert({
                                  title: 'Success Message',
                                  template: message
                                });
                          $scope.empty = {};
                          $scope.serviceData = angular.copy($scope.empty);
                          $timeout(function() {
                            window.location.reload();
                          }, 1000);
  
                       } 
                  //alert(JSON.stringify(result));
                }, function(err) {
                  //$ionicLoading.hide();
                  $cordovaToast.showLongBottom('Error : ' + err);
                   alert(JSON.stringify(err));
                });
          /* var user = $localstorage.getObject('user');   
           serviceData.service_customer = user.id;
           serviceData.image = $scope.model_image; 
           console.log("user"+JSON.stringify(user));
          factory.post_List_obj(url+"/new_service_listing",serviceData).then(function(result){
                      // $ionicLoading.show({duration:3000,template:'<img src="../img/logo.png">'});
                      $ionicLoading.hide();
                       if(result.status == 'error'){
                        $scope.results = result;
                             $ionicPopup.alert({
                                  scope : $scope,
                                  title: 'Register Message',
                                  templateUrl: "templates/popup.html"
                                });
                       }else{
                          $ionicPopup.alert({
                                  title: 'Register Message',
                                  template: result.message
                                });
                          $scope.empty = {};
                          $scope.serviceData = angular.copy($scope.empty);
                       }
                      
           });*/
       }

       $scope.loadImage = function() {
          var showActionSheet = $ionicActionSheet.show({
            buttons: [{
                text: 'Choose from Gallery'
              }/*,
              {
                text: 'Use Camera'
              }*/
            ],
            // destructiveText: 'Delete',
            titleText: 'Select Image Source',
            cancelText: 'Cancel',
            cancel: function() {

            },

            buttonClicked: function(index) {
              var type = null;
              if (index === 0) {
                type = Camera.PictureSourceType.PHOTOLIBRARY;
              }
              if (index === 1) {
                type = Camera.PictureSourceType.CAMERA;
              }
              if (type !== null) {
                $scope.selectPicture(type);
              }
              return true;
            },

            destructiveButtonClicked: function() {
              // add delete code..
            }
          });
        };

    // Returns the local path inside the app for an image
  $scope.pathForImage = function(image) {
    if (image === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + image;
    }
  };
  var temp = {};
     $scope.selectPicture = function(sourceType) {
    var options = {
      quality: 100,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      saveToPhotoAlbum: false
    };

    $cordovaCamera.getPicture(options).then(function(imagePath) {
        // Grab the file name of the photo in the temporary directory
        var currentName = imagePath.replace(/^.*[\\\/]/, '');

        //Create a new name for the photo
        var d = new Date(),
          n = d.getTime(),
          newFileName = n + ".jpg";

        // If you are trying to load image from the gallery on Android we need special treatment!
        if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
          window.FilePath.resolveNativePath(imagePath, function(entry) {
            window.resolveLocalFileSystemURL(entry, success, fail);

            function fail(e) {
              console.error('Error: ', e);
            }

            function success(fileEntry) {
              var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
              // Only copy because of access rights
              $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success) {
                $scope.model_image = newFileName;
                /*$ionicLoading.show({
                  template: '<ion-spinner icon="ios" class="spinner-assertive"></ion-spinner>'
                })
                
                // Destination URL
                var urls = url + '/upload_ad_request_image';
                // File for Upload
                var targetPath = $scope.pathForImage($scope.model_image);
                // File name only
                var filename = $scope.model_image;
                var options = {
                  fileKey: "file",
                  fileName: filename,
                  chunkedMode: false,
                  mimeType: "multipart/form-data",
                  params: {
                    'fileName': filename
                  }
                };*/
               /* $cordovaFileTransfer.upload(urls, targetPath, options).then(function(result) {
                  $ionicLoading.hide();
                  $cordovaToast.showLongCenter("Your photo has been uploaded");
                 // alert(JSON.stringify(result));
                }, function(err) {
                  $ionicLoading.hide();
                  $cordovaToast.showLongBottom('Error : ' + err);
                   alert(JSON.stringify(err));
                });*/
              }, function(error) {
                $cordovaToast.showLongBottom('Error : ' + error.exception);
              });
            };
          });
        } else {
          var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          // Move the file to permanent storage
          $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success) {
              $scope.model_image = newFileName;
           /* $ionicLoading.show({
              template: '<ion-spinner icon="ios" class="spinner-assertive"></ion-spinner>'
            })*/
            // Destination URL
           /* var urls = url + '/upload_ad_request_image';
            // File for Upload
            var targetPath = $scope.pathForImage($scope.model_image);
            // File name only
            var filename = $scope.model_image;
            var options = {
              fileKey: "file",
              fileName: filename,
              chunkedMode: false,
              mimeType: "multipart/form-data",
              params: {
                'fileName': filename
              }
            };*/

            /*$cordovaFileTransfer.upload(urls, targetPath, options).then(function(result) {
              $ionicLoading.hide();
              $cordovaToast.showLongCenter("Your photo has been uploaded");
            }, function(err) {
              $ionicLoading.hide();
              $cordovaToast.showLongBottom('Error : ' + err);
            });*/
          }, function(error) {
            $cordovaToast.showLongBottom('Error : ' + error.exception);
          });
        }
      },
      function(err) {
        // Not always an error, maybe cancel was pressed...
      })
  };

})
app.controller('notificationDetailsCntrl', function($scope, $stateParams,factory,$ionicLoading) {
     var id = $stateParams.id;
     factory.post_List_obj(url+"/show_notification",{'id':id}).then(function(result){
                  // $ionicLoading.show({duration:3000,template:'<img src="../img/logo.png">'});
                  $ionicLoading.hide();
                  $scope.listings = result.list;
                  
       });
});
