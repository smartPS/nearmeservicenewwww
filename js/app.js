// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

var message_url = "https://listing.schoolmanagementsoftware.tech/device_register"; 

var main_url = "https://listing.schoolmanagementsoftware.tech/";
var url = "https://listing.schoolmanagementsoftware.tech/api";
var url_api = "https://listing.schoolmanagementsoftware.tech/api/data";

/*var main_url = "http://test/";
var url = "http://test/api";
var url_api = "http://test/api/data";*/

var app = angular.module('starter', ['ionic', 'ngCordova', 'ionic-material','cordovaGeolocationModule']);

app.run(function($ionicPlatform,$localstorage,location,factory,$ionicLoading,$localstorage,cordovaGeolocationService,$cordovaGeolocation,$state,$ionicPopup) {
  $ionicPlatform.ready(function() {
        
        factory.get_List(url+"/get_today_day").then(function(result){
                       $ionicLoading.hide();
                      if(result.status == 'success'){
                        $ionicPopup.alert({
                            title: 'Special day',
                            template: result.day
                         });
                      }
         });
        
            /*$ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Acquiring location!'
            });*/
          cordova.plugins.diagnostic.isLocationEnabled(function(enabled){
            cordova.plugins.locationAccuracy.canRequest(function(canRequest){
              if(canRequest){
                  cordova.plugins.locationAccuracy.request(function (success){
                      //alert("Successfully requested accuracy: "+success.message);
                      
                      
                  }, function (error){
                     console.error("Accuracy request failed: error code="+error.code+"; error message="+error.message);
                     if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
                         if(window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")){
                             cordova.plugins.diagnostic.switchToLocationSettings();
                         }
                     }
                  }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
              }
          });
          }, function(error){
              alert("The following error occurred: "+error);
          });


   

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

        push = PushNotification.init({
            android: {
                senderID: "940476695916",
                sound: "true",
                vibrate: "true",
                forceShow: "true"
            }
        });

        push.on('registration', function(data) {
             
              var model=ionic.Platform.device();
              var app_id = "bqfknctaXU0nf7k1Lz4CAc2aa";
              var obj={'app_id':app_id,'device_token':data.registrationId,'model':model.model,'platform':model.platform,'status':true}
                  $localstorage.set('device_token',data.registrationId);
                //alert("device token" + JSON.stringify(obj));
                 //alert("device token" + JSON.stringify(obj));
                  var getLoc =  $localstorage.getObject('getLoc');
                     //alert("lonlat" + JSON.stringify(getLoc));
                  getLoc.device_id = data.registrationId;
                  getLoc.model = model.model;
                // alert("initial geo" + JSON.stringify(getLoc));
                 /*Store to notification panel*/
                 factory.post_List_obj(message_url,obj).then(function(result){
                       $ionicLoading.hide();
                      // alert("msg"+JSON.stringify(result))
                      
                });

                 /*store to our panel*/
                 // alert("initial geo" + JSON.stringify(getLoc));
                 factory.post_List_obj(url+"/set_geo",getLoc).then(function(result){
                       $ionicLoading.hide();
                       //alert("hub"+JSON.stringify(result))
                      
                });

        });

       
         push.on('notification', function(data) {
          //var linked = data;
          //var linked = data.additionalData.additionalData;
          var a=data.additionalData.additionalData.split("_");
          /*if(a[0]=="special"){
            $state.go('app.special',{'obj':{'page':a[0],'id':a[1]}});
          }*/
           //alert(JSON.stringify(data))
           $state.go('app.'+a[0],{'id':a[1]});
        });  

         

       

       
  });
})

app.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'searchCntrl'
      }
    }
  })
   .state('app.post_listing', {
    url: '/post_listing',
    views: {
      'menuContent': {
        templateUrl: 'templates/post_listing.html',
        controller: 'post_listingCntrl'
      }
    }
  })

  .state('app.listing', {
      url: '/listing/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/listing.html',
          controller: 'listingCntrl'
        }
      }
    })
  .state('app.listingDetail', {
      url: '/listingDetail/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/listingDetail.html',
          controller: 'listingDetailCntrl'
        }
      }
    })
    .state('app.ownListingDeatils', {
      url: '/ownListingDeatils/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/ownlistingDetail.html',
          controller: 'ownlistingDetailCntrl'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

    .state('app.tourists', {
      url: '/tourists',
      views: {
        'menuContent': {
          templateUrl: 'templates/tourists.html',
          controller: 'touristsCntrl'
        }
      }
    })

  .state('app.tourist', {
    url: '/tourist/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/tourist.html',
        controller: 'touristCntrl'
      }
    }
  })
  
  .state('app.special', {
      url: '/special/:spl',
      views: {
        'menuContent': {
          templateUrl: 'templates/special.html',
          controller: 'specialCntrl'
        }
      }
    })
  .state('app.news', {
      url: '/news',
      views: {
        'menuContent': {
          templateUrl: 'templates/news.html',
          controller: 'newsCntrl'
        }
      }
    })
  .state('app.newsDetails', {
      url: '/newsDetails/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/newsDetails.html',
          controller: 'newsDetailsCntrl'
        }
      }
    })
  .state('app.notificationDetails', {
      url: '/notificationDetails/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/notificationDetails.html',
          controller: 'notificationDetailsCntrl'
        }
      }
    })
  .state('app.specialDetail', {
    url: '/specialDetail/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/specialDetail.html',
        controller: 'specialDetailCntrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/search');

});

app.factory('$localstorage', ['$window', function ($window) {
  return {
    set: function (key, value) {
      $window.localStorage[key] = value;
    },
    get: function (key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function (key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function (key) {
      return JSON.parse($window.localStorage[key] || '{}');
    },
    remove: function(key) {
      $window.localStorage.removeItem(key);
    }
  }
}])
app.factory('location', function($localstorage,$ionicLoading) {
 // $ionicLoading.show();
  var options = {enableHighAccuracy: true ,timeout :100000};
  return {
    set: function(){
      return navigator.geolocation.getCurrentPosition(function(pos) {
       //$localstorage.setObject('getLoc',{'latitude':pos.coords.latitude,'longitude' : pos.coords.longitude});
       //$ionicLoading.hide(); 
      },function(error) {
       
      }, options);
    }
  }
})
app.factory('factory',function($http,$log,$q,$ionicLoading,$filter){
  return{
    get_List:function(url){
    $ionicLoading.show();  
    var deferred = $q.defer();
     $http.get(url)
     .success(function(data){
      deferred.resolve(data);
     }).error(function(msg,code){
      deferred.reject(msg);
          $log.error(msg, code);
     });
     return deferred.promise;
  } ,
  post_List:function(url,id){
    var deferred = $q.defer();
     $http.post(url,{'id':id})
     .success(function(data){
      deferred.resolve(data);
     }).error(function(msg,code){
      deferred.reject(msg);
          $log.error(msg, code);
     });
     return deferred.promise;
  },
  post_List_obj:function(url,obj){
    var deferred = $q.defer();
     $ionicLoading.show();  
     $http.post(url,obj)
     .success(function(data){
      deferred.resolve(data);
     }).error(function(msg,code){
      deferred.reject(msg);
          $log.error(msg, code);
     });
     return deferred.promise;
  },
  social:function(source,item){
       var des = $filter('limitTo')(item.description, 200) + '...';
       var message = "["+item.title+"]" + " : "+ des; 
       var image = item.images[0].image;

      var link = "https://play.google.com/store/apps/details?id=com.spsolutions.nearmeservices";
      if(source == 'whatsapp'){
             window.plugins.socialsharing.shareViaWhatsApp(message, image, link , null, function(errormsg){alert("Error: Cannot Share")});
      }else if(source == 'other'){
             window.plugins.socialsharing.share(message, image, null, link);
      }
    }


  }

})
app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
})
app.filter('customSplit', function() {
  return function(input) {
    console.log(input);
    var ar = input.split(','); // this will make string an array 
    return ar;
  };
});